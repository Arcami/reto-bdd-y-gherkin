# Language es

Característica: Cuenta de ahorros en sucursal virtual
yo como usuario
Necesito un cuenta de ahorros en el banco
Para usar servicios en sucursal virtual


Antecedentes: 
Dado que el usuario ingresó a la sección cuenta de ahorros

Escenario: Detalle del producto
Cuando el usuario oprime la opción ver detalle del producto
Entonces el usuario podrá ver los detalles de la cuenta

Escenario: Visualizar los movimientos de cuenta
Cuando el usuario oprime la opción ver movimientos
Entonces se mostrarán los movimientos que ha realizado


Antecedentes:
Dado que el usuario está en la sección de transferencia de dinero

Escenario: Transferir dinero a cuentas inscritas
Cuando el usuario selecciona la cuenta que tenga inscrita
Entonces el usuario enviará dinero a la cuenta destino

Escenario: Transferir dinero a cuentas no inscritas
Cuando el usuario ingresa la información de la cuenta receptora
Entonces el usuario enviará dinero a la cuenta destino


Antecedentes: 
Dado que el usuario está en la sección de pagos

Escenario: Pagar tarjetas
Cuando el usuario selecciona la tarjeta que desea pagar
Entonces el usuario pagará la tarjeta seleccionada

Escenario: Pagar créditos
Cuando el usuario selecciona el crédito a pagar
Entonces el usuario pagará el crédito seleccionado

Escenario: Pagar facturas
Cuando el usuario selecciona la factura a pagar
Entonces el usuario pagará la factura seleccionada

